
#!/bin/env python
from collections import deque
from pprint import pprint
import gene_ontology as gom
##TP1--------------------
def create_graph(directed = True, weighted = False, weight_attribute = None):
    
    """
    cette fonction renvoie un dictionnaire qui correspond au graphe.
    en paramètre on peut rajouter des attributs tels que l'orientation du graphe,
    s'il existe des valuations ou pas ainsi que le poids de ces valuations'
    """
    g = { 'nodes': {}, 'edges': {}, 'directed': directed, 'weighted': weighted, 'weight_attribute': weight_attribute }
    return g
print("# Graph lib tests")
print("## create_graph")
g = create_graph()
pprint(g)

##### main → tests #####
if __name__ == "__main__":
    print("# Graph lib tests")
    print("## create_graph")
    g = create_graph()
    pprint(g)
    
def add_node(g, node_id, attributes = None): 
    """
    cette fonction rajoute un sommet en mettant en paramètre le graphe à rajouter,
    ainsi que l'identifiant du sommet, les attributs du sommet peuvent être
    données sous forme de dictionnaire.
    
    """
    if node_id not in g['nodes']: # On vérifie si ce sommet n'existe pas déja dans le graphe
        if attributes is None: # crée un attribut null s'il n'a pas été rajouté en paramètres
            attributes = {}
        g['nodes'][node_id] = attributes
        g['edges'][node_id] = {} # initialises le dictionnaire de voisins adjacents
    return g['nodes'][node_id] # renvoie le sommet dans le dictionnaire 'nodes'
def add_edge(g, node_id1, node_id2, attributes = None, node1_attributes = None, node2_attributes = None): 
    # crée le sommet s'il n'existe pas dans le dictionnaire
    if node_id1 not in g['nodes']: add_node(g, node_id1, node1_attributes) # vérifie que node1 existe
    if node_id2 not in g['nodes']: add_node(g, node_id2, node2_attributes) # vérifie que node2 existe
    #rajoute le(s) arcs seulement s'il(s) n'exise(nt) pas 
    if node_id2 not in g['edges'][node_id1]:
        if attributes is None: # crée un attribut null s'il n'a pas été rajouté en paramètres
            attributes = {}
        g['edges'][node_id1][node_id2] = attributes
        if not g['directed']:
            g['edges'][node_id2][node_id1] = g['edges'][node_id1][node_id2] # partage le même attribut que n1->n2
    return g['edges'][node_id1][node_id2] # renvoie l'arc n1->n2 dans le dictionnaire 'edge'

print("## add nodes and edges")
g = create_graph()
add_node(g, 'A')
add_node(g, 'B')
add_edge(g, 'A', 'B', { 'weight': 5 } )
pprint(g)

def read_delim(filename, column_separator="\t", directed=True, weighted=False, weight_attribute=None): 
    """
    Analyse un fichier texte dont les colonnes sont séparées par le séparateur de colonnes spécifié et 
    renvoie un graphe.
    
    line syntax: node_id1   node_id2    att1    att2    att3    ...
    """
    g = create_graph(directed, weighted, weight_attribute)
    with open(filename) as f: 
        # récupère le nom des colonnes 
        tmp = f.readline().rstrip()
        attNames= tmp.split(column_separator)
        # SUPPRIME LES DEUX PREMIÈRES COLONNES QUI CORRESPONDENT AUX ÉTIQUETTES DES SOMMETS CONNECTÉS
        attNames.pop(0)  # enlève la première colonne nom (le noeud source ne doit pas figurer dans les noms d'attributs)
        attNames.pop(0)  # enlève la deuxième colonne (noeud cible ...)
        # TRAITER LES LIGNES RESTANTES
        row = f.readline().rstrip()
        while row:
            vals = row.split(column_separator)
            u = vals.pop(0)
            v = vals.pop(0)
            att = {}
            for i in range(len(attNames)):
                att[ attNames[i] ] = vals[i]
            add_edge(g, u, v, att)
            row = f.readline().rstrip() # PROCHAINE LIGNE
        return g
def nb_nodes(g):
    """
    Cette fonction renvoie le nombre de sommet que compte un graphe en utilisant la fonction len sur le dictionnaire 'nodes'
    se trouvant dans le graphe g
    """
    sommet=len(g['nodes'])
    return("le nombre de sommet est=",sommet)
sommet=nb_nodes(g)
pprint(sommet)
def nb_edges(g):
    """
    Cette fonction renvoie le nombre d'arcs que compte un graphe en créant un objet nb_arete puis
    incrémente cet objet au fur et a mesure qu'on trouve des arcs dans le dictionnaire 'edges'.
    se trouvant dans le graphe g
    """
    nb_aretes=0
    for node_id1 in g['edges']:
        for node_id2 in g['edges'][node_id1]:
            nb_aretes+=1
    return("le nombre d'arètes est= ",nb_aretes)

arc=nb_edges(g)
pprint(arc)
def node_exist(g,node_id):
    """
    Cette fonction cherche si un sommet existe en le cherchant directement dans le dictionnaire 'nodes' du graphe g
    """
    if node_id in g['nodes']:
        print("oui le sommet existe")
    else:
        print('ce sommet n existe pas ')
node_exist(g,'A')
node_exist(g,'Z')

def edge_exist(g,node_id1,node_id2):
    """
    Cette fonction cherche si un arc existe en le cherchant directement dans le dictionnaire 'edges' du graphe g
    """
    if node_id2 in g['edges'][node_id1]:
        print("oui cet arc existe")
    else:
        print("cet arc n'existe pas")
edge_exist(g, 'A', 'B')
edge_exist(g,'A','Z')
def neighbors(g,node_id):
    """
    Cette fonction renvoie la liste des voisins d'un sommet en cherchant dans le dictionnaire 'edges', le nombre de sommets 
    auxquels celui ci est adjacent. En utilisant la formulation g['edges'][node_id.keys(), on récupère les voisins auxquels 
    ce sommet est adjacent et on les rajoute dans une liste.
    """
    liste=[]
    for voisins in g['edges'][node_id].keys():
        liste.append(voisins)
    return(liste)
voisin=neighbors(g,'A')
pprint(voisin)


def bfs(graph, start_node):
    """
    Cette fonction permet de réaliser un parcours en largeurs (BFS) d'un graphe en créant une file qu'on va vider en fonction 
    du premier sommet rajouté.
    """
    states={}   #on initialise un dictionnaire état (states) et un dicionnaire distance
    distance={}
    for sommet in graph['nodes']:
        states[sommet]='Unexplored'
    for sommet in graph['nodes']:
        distance[sommet]="inf"
    #pour tous les sommets on met l'etat unexeplored et infini a la distance
    predecessors={start_node:None}# on crée un dictionnaire prédecessor ayant comme clé la source et comme valeur None
    to_visit=[start_node]
    visited=[]
    distance[start_node]=0
    while to_visit: # tant que la liste to_visit n'est pas null
        current_node=to_visit.pop(0) #on prend le premier élément de la liste to visit,
        states[current_node]="Discovered"# on met a son état découvert 
        if current_node not in visited:
            visited.append(current_node) # on rajoute ce sommet à la liste découvert s'il n'y est pas déja 
            for neighbor in graph['edges'][current_node]:#puis on récupère les sommets adjacents au sommet actuel,
                if neighbor not in predecessors:#on rajoute ces sommets au dictionnaire prédécesseurs et ils auront comme valeur le sommet actuel
                    predecessors[neighbor] = current_node
                    distance[neighbor]=distance[current_node]+1#on rajoute à ces sommets adjacents au dictionnaire distance en leur attribuant comme valeur la distance du sommet actuel+1
                    to_visit.append(neighbor)#enfin on rajoute les sommets adjacents à la liste to_visit afin de continuer le parcours
 
        dico_bfs={"source":start_node,"distance":distance,
                  "predecessor":predecessors,"states":states,}
    return(dico_bfs)

def chemin(graph, start_node, end_node):
    """
    Cette fonction permet de récuperer un chemin en partant d'un sommet start jusqu'à un sommet end, 
    on se base sur le dictionnaire predecessor retourné par le BFS pour récuperer ce chemin 
    """
    dico = bfs(graph, start_node)
    chemin = [end_node]
    current = end_node
    #Tant que le sommet actuel est différent du sommet start, on continue la recherche
    while current != start_node:
        print(current)
        current = dico["predecessor"][current]#cette ligne de code permet de récuperer le predecesseur du sommet actuel
        #à la place du sommet actuel ainsi de suite jusqu'à arriver au sommet start
        chemin.append(current)
    return chemin[::-1]  # Inverser le chemin pour l'ordre correct

    
def induced_subgraph(g, nodes):
  """
  Cette fonction permet de creer un sous graphe induit d'un graphe a partir d'une liste de sommets 
  """  

  sg = create_graph(g['directed'], g['weighted'], weight_attribute=g['weight_attribute'])

  for noeuds in nodes:
      add_node(sg,noeuds)# pour commencer on rajoute les sommets ce qui est facile avec add_nodes
  for noeud in nodes: 
  #ensuite on rajoute les voisins aux sommets grace à la fonction neighbors 
      voisin = neighbors(g, noeud)
      #après avoir récupérer la liste des voisins, j'refais une itération afin de rajouter a chaque noeud 
      #ses voisins dans le graphe sg
      for element in voisin:
          add_edge(sg, noeud, element)
    #tant que la liste voisin n'est pas vide, j'retire le 1er element puis je cherche ses voisins
  while voisin:
      current=voisin.pop(0)
      voisins=neighbors(g,current)
    #je rajoute les voisins du voisin dans la liste voisin et j'crée les arcs du voisin reliés à ses voisins
      for elements in voisins:
          voisin.append(elements)
          add_edge(sg,current,elements)
          
  return (sg)


def clustering_coefficient(g, node_id):
    """
    Cette fonction permet de calculer le coefficient d'agglomération du sommet d'un graphe 
    """  
    neighbors_list = list(neighbors(g, node_id))
    voisin=0
    for neighbor in neighbors(g,neighbors_list):
        voisin+=1
    nbr_voisins=neighbors_list.count()
    voisin_possible=nbr_voisins*(nbr_voisins-1)/2
    clustering=voisin/voisin_possible
    return(clustering)
### TP3 ---------------------------------------------------------------------------
def dfs(g):
    """
    Cette fonction permet de réaliser un parcours en profondeur d'un graphe (DFS)
    """  
    #On commence par créer un graphe qu'on va ajouter au graphe donné au paramètre 
    G = {'state': {},
         'predecessor': {},
         'time': 0,
         'classification': {},
         'discovered': {},
         'finished': {}
         }
    g['dfs'] = G
    #on initialise les dictionnaires du graphe DFS
    for node in g['nodes']:
        G["state"][node] = "UNEXPLORED"
        G['predecessor'][node] = "NONE"
    G['time'] = 0
    for node in g["nodes"]:
        if G['state'][node] == "UNEXPLORED":
            dfs_visit(g, node)
    return(g['dfs'])


def dfs_visit(g, noeud):
    """
    On programme une fonction dfs_visit qui va permettre de réaliser  le parcours du DFS
    """  
    #on récupère le graphe qu'on a initialisé précedemment 
    G = g['dfs']
    #pour le noeud actuel on met un statut découvert et on incrémente +1 sur le dictionnaire temps a chaque fois qu'on réalise le dfs_visit
    G['state'][noeud] = "DISCOVERED"
    G['time'] += 1
    G['discovered'][noeud] = G['time'] # on rajoute au dictionnaire discovered du noeud le temps auquel on l'a découvert 
    #ceci nous aidera pour classifier les noeuds
    for voisin in g['edges'][noeud]:#pour les voisins du noeud actuel 
        if G['state'][voisin] == 'UNEXPLORED':
            G['predecessor'][voisin] = noeud
            G['classification'][noeud, voisin] = "TREE EDGE"#si le voisin n'a pas été exploré alors l'arc est classifié de tree edge(parcours normal)
            dfs_visit(g, voisin)
        elif G['state'][voisin] == 'DISCOVERED':#si le voisin a déja été découvert mais n'est pas terminé alors l'arc est classifié comme
        #Back EDGE  ce qui sous entend que ce graphe est cyclique
            G["classification"][noeud, voisin] = "BACK EDGE"
        elif G["discovered"][noeud] > G["discovered"][voisin]:#par contre si le voisin est classé comme finished mais que son temps de 
        #discovered est inférieur au temps de discovered du noeud actuel alors cet arc est classifié comme CROSS EDGE
            G["classification"][noeud, voisin] = "CROSS EDGE"
        else:#si le temps de discovered du voisin est supérieur au noeud actuel alors cet arc est classifié comme Forward Edge 
            G["classification"][noeud, voisin] = "FORWARD EDGE"
    G["state"][noeud] = "FINISHED"
    G['time'] += 1
    G["finished"][noeud] = G['time']
def is_acyclic(g):
    """
    Cette fonction permet de savoir si un graphe est acyclique ou pas, en cherchant dans la classification des arcs s'il existe
    un Back Edge.
    """  
    dfs(g)#on commence par réaliser un dfs du graphe
    for arcs in g['dfs']['classification'].values():#on parcourt le dictionnaire classification du dfs et si un arc correspond à
    #un back edge alors il existe un cycle dans ce graphe
        if arcs=="BACK EDGE":
            return("il existe un cycle dans ce graphe")
        
    return("ce graphe est acyclique")
graphe_cycle=read_delim('directed.graph.with.cycle.tsv')
print(is_acyclic(graphe_cycle))
def tri_topologique(g):
    """
    Cette fonction permet de faire un tri topologique des éléments du graphe
    """  
    dfs(g)#on commence par réaliser un dfs du graphe
    liste=[]#on initialise une liste sur laquelle on rajoute les sommets du dictionnaire finished ainsi que leur temps
    for sommet, ordre in g['dfs']['finished'].items():
        liste.append((sommet,ordre))
    liste=liste[::-1]
    return(liste)


    