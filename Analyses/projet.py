# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 13:18:25 2023

@author: Mbow
"""


import graphmaster as gm 
import gene_ontology as gom
from pprint import pprint
parasite=gom.load_OBO('\\Data\\go-basic.obo')
gom.load_GOA2(parasite,'\\Data\\2325912.P_parasitica_CBS_412.66.goa')
gm.nb_edges(parasite)
gm.nb_nodes(parasite)
def GOTerms(go, gp_id, recursive=False):
    if not recursive:
        if gp_id in go['nodes']: 
            return gm.neighbors(go, gp_id)
    else:
        go['bfs'] = gm.bfs(go, gp_id)
        gots = []
        for goterm in go['bfs']['predecessor'].keys():
            if go['nodes'][goterm]['type']=='GOTerm':
             gots.append(goterm)
        return gots
        return None 
    return None

def GeneProduct(go,go_term,recursive=False):
    if not recursive:
        liste=[] 
        for neighbour in go['edges']:
            if go_term in go['edges'][neighbour] and go['nodes'][neighbour]['type']=='GeneProduct':
                liste.append(neighbour)
        return(liste)
    else:
        #je récupère une liste des GOTERMs reliés au GOTerm clé
        liste=[go_term]
        go['bfs']=gm.bfs(go,go_term)
        for go_term in go['bfs']['predecessor'].keys():
            if go_term not in liste:
                liste.append(go_term)
        print('liste=',liste)
        geneproduct=[]
        for element in liste:
            for neighbour in go['edges']:
                if element in go['edges'][neighbour] and go['nodes'][neighbour]['type']=='GeneProduct':
                    geneproduct.append(neighbour)
        return(geneproduct)

def MaxDepth_3_ontologies(go):
    dist_bio = 0
    dist_cell=0
    dist_mol=0
    sommetdepart_bio=""
    sommetfinal_bio=""
    sommetdepart_cell=""
    sommetfinal_cell=""
    sommetdepart_mol=""
    sommetfinal_mol=""

    for sommet in go["nodes"]:
        if go['nodes'][sommet]['type']=='GOTerm' and go['nodes'][sommet]['namespace']=='biological_process':      
            bfs_smt = gm.bfs(go, sommet)
            distance_smt = bfs_smt["distance"]
            for noeud, valeur in distance_smt.items():
                if valeur != 'inf' and int(valeur) > dist_bio:
                    sommetdepart_bio=sommet
                    sommetfinal_bio=noeud
                    dist_bio = int(valeur)
        elif go['nodes'][sommet]['type']=='GOTerm' and go['nodes'][sommet]['namespace']=='molecular_function':      
            bfs_smt = gm.bfs(go, sommet)
            distance_smt = bfs_smt["distance"]
            for noeud, valeur in distance_smt.items():
                if valeur != 'inf' and int(valeur) > dist_mol:
                    sommetdepart_mol=sommet
                    sommetfinal_mol=noeud
                    dist_mol = int(valeur)
        elif go['nodes'][sommet]['type']=='GOTerm' and go['nodes'][sommet]['namespace']=='cellular_component':      
            bfs_smt = gm.bfs(go, sommet)
            distance_smt = bfs_smt["distance"]
            for noeud, valeur in distance_smt.items():
                if valeur != 'inf' and int(valeur) > dist_cell:
                    sommetdepart_cell=sommet
                    sommetfinal_cell=noeud
                    dist_cell = int(valeur)
    print('la profondeur du biological_process=',dist_bio,"le chemin est=",gm.chemin(go, sommetdepart_bio, sommetfinal_bio),"\n"
          ,'la profondeur de la molecular_function=',dist_mol,"le chemin MF est=",gm.chemin(go,sommetdepart_mol,sommetfinal_mol),"\n"
          ,'la profondeur du cellular_component=',dist_cell,"le chemin est=",gm.chemin(go,sommetdepart_cell,sommetfinal_cell))
def nbr_geneproduct(go):
    nombre_go=0
    nombre_gp=0
    for element in go['nodes']:
        if go['nodes'][element]['type']=='GOTerm':
            nombre_go+=1 
        else:
            nombre_gp+=1
    print('le nombre de goterm=',nombre_go,'le nombre de geneproduct=',nombre_gp)
#################### TEST DES SCRIPTS REALISEES#####################################################
nbr_geneproduct(parasite)
MaxDepth_3_ontologies(parasite)
GeneProduct(parasite,'GO:0031175',recursive=False)
GeneProduct(parasite,'GO:0031175',recursive=True)
print(GOTerms(parasite,'A0A0B7MLW7',recursive=False))
print(GOTerms(parasite,'A0A0B7MLW7',recursive=True))

