# Projet Gene Ontology - Phytophthora parasitica

## Introduction

Ce projet vise à exploiter et améliorer l'exploration de la Gene Ontology pour l'organisme *Phytophthora parasitica* en utilisant un module Python dédié (`geneontology.py`). Cet organisme pathogène est responsable de maladies dans de nombreuses plantes, et l'étude de sa Gene Ontology peut aider à comprendre et limiter son pouvoir pathogène.

## Objectifs

- Chargement d'un graphe représentant la Gene Ontology.
- Développement de fonctions pour analyser la profondeur des ontologies.
- Extraction des GeneProducts associés aux GOTerms et vice versa.

## Conception du Graphe

Le graphe est représenté sous forme de dictionnaire avec les clés suivantes :
- **Nodes** : Contient les sommets avec des informations comme le type de GOTerm, son nom, sa définition, et son domaine.
- **Edges** : Décrit les liaisons entre les sommets (types `is a`, `part of`).
- **Directed** : Indique si le graphe est orienté (oui dans notre cas).
- **Weighted** : Indique si le graphe est valué (non dans notre cas).

## Fonctions Développées

### 1. Obtention de la Profondeur Maximale des Ontologies

Cette fonction calcule la profondeur maximale des trois ontologies (`biological_process`, `molecular_function`, `cellular_component`) en utilisant un parcours en largeur (BFS).

### 2. Extraction des GeneProducts Associés à un GOTerm

Permet de récupérer les GeneProducts associés à un GOTerm donné, avec une option pour inclure les descendants.

### 3. Extraction des GOTerms Associés à un GeneProduct

Permet de récupérer les GOTerms associés à un GeneProduct donné, avec une option pour inclure les descendants.

## Résultats

- Le graphe de *Phytophthora parasitica* contient 50,016 sommets et 103,274 arcs.
- Le génome étudié compte 42,769 GOTerms et 7,247 GeneProducts.
- 97% des GOTerms sont annotés, avec seulement 1,157 GOTerms non annotés.

### Performance des Fonctions

- **Profondeur Maximale** : Exécution en 15 minutes pour un graphe de 50,016 sommets.
- **GeneProducts Associés** : Instantané en mode non récursif; 5 secondes en mode récursif pour 20 descendants.
- **GOTerms Associés** : Instantané en mode non récursif; retourne 44 résultats en mode récursif.

## Discussion

Les résultats obtenus montrent une bonne annotation et la pertinence des fonctions développées pour explorer la Gene Ontology de *Phytophthora parasitica*. Des améliorations futures incluent l'optimisation de la fonction de profondeur maximale et l'utilisation d'un fichier `.obo` plus spécifique pour une meilleure annotation.

## Lien GitLab

Consultez le code et les données sur notre [GitLab](https://gitlab.com/oumarklf/projet_gene_ontology).

---

## Annexe

### Fonction load_GOA

```python
# Fonction modifiée pour compter les GOTerms annotés et non annotés
def load_GOA():
    # Initialisation des compteurs
    annotated_count = 0
    unannotated_count = 0
    # Traitement des données
    ...
    return annotated_count, unannotated_count
